package com.epam;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;


class Main {


    public static void main(String[] args) throws IntrospectionException {

        System.out.println("nazwa klasy: ");
        System.out.println(MyEnum.class.getName());
        System.out.println();
        System.out.println("nazwy pól: ");
        Arrays.stream(MyEnum.class.getDeclaredFields())
                .map(Field::getName)
                .forEach(System.out::println);
        System.out.println();

        System.out.println("parametry pól:");

        for (Field f : MyEnum.class.getDeclaredFields()) {
            System.out.println("nazwa "+f.getName());
            System.out.println("typ "+f.getType());
            System.out.println("kanoniczna nazwa typu "+f.getType().getCanonicalName());
            System.out.println("Statyczne " + Modifier.isStatic(f.getModifiers()));
            System.out.println("prywatne " + Modifier.isPrivate(f.getModifiers()));
            System.out.println("publiczne " + Modifier.isPublic(f.getModifiers()));
            System.out.println("final " + Modifier.isFinal(f.getModifiers()));
            System.out.println();
        }
        System.out.println("i można wyciągnąć wiele wiele innych parametrów....");
        System.out.println();

        System.out.println("metody wszystkie:");
        Arrays.stream(MyEnum.class.getMethods())
                .map(Method::getName)
                .forEach(System.out::println);
        System.out.println();


        System.out.println("metody zadeklarowane:");
        Arrays.stream(MyEnum.class.getDeclaredMethods())
                .map(Method::getName)
                .forEach(System.out::println);
        System.out.println();


        System.out.println("wszystkie metody i ich parametry:");
        for (Method f : MyEnum.class.getMethods()) {
            System.out.println("nazwa: "+f.getName());
            System.out.println("ilość parametrów wejściowych: "+f.getParameterCount());
            System.out.println("typ zwracany: "+f.getReturnType());
            System.out.println("statyczne: " + Modifier.isStatic(f.getModifiers()));
            System.out.println("prywatne: " + Modifier.isPrivate(f.getModifiers()));
            System.out.println("publiczne: " + Modifier.isPublic(f.getModifiers()));
            System.out.println("final: " + Modifier.isFinal(f.getModifiers()));
            System.out.println();

        }

        System.out.println(MyEnum.class.getDeclaredFields()[3].getType());




    }

}

