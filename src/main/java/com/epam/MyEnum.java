package com.epam;

public enum MyEnum {

    X {
        @Override
        void m() {

        }
    },
    Y {
        @Override
        void m() {

        }
    },
    Z {
        @Override
        void m() {

        }
    };

    abstract void m();
}
